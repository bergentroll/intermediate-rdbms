'''
Модуль отвечает за работу и отображения главной вкладки
'''

import config
from tkinter import ttk
from lib.database import Table
from tkinter import messagebox
from lib.dynamic_entries import DynamicEntries


class TabMain:
    padding = config.padding

    def update_text(self):
        '''
        Этот метод должен быть переназначен в главном модуле
        '''
        pass

    def __init__(self, ancestor, update_text):
        self.update_text = update_text
        self.tab = ttk.Frame(ancestor, relief='flat')
        self.tables_frame = \
            ttk.LabelFrame(
                self.tab,
                relief='ridge',
                text='Выбор таблицы'
                )
        self.tables_list = ttk.Combobox(self.tables_frame, state='readonly')
        self.tables_list['values'] = Table.database.tables
        self.tables_list.current(0)
        self.table = Table(self.tables_list.get())
        self.tables_list.bind('<<ComboboxSelected>>', self.tables_list_handler)
        self.edit_frame = ttk.LabelFrame(
                self.tab, relief='ridge',
                text='Редактирование таблицы'
                )
        self.table_dynamic_entries_frame = \
            ttk.Frame(self.edit_frame, relief='flat')
        self.edit_buttons_frame = ttk.Frame(self.edit_frame, relief='flat')
        self.add_entry_button = ttk.Button(
            self.edit_buttons_frame, text='Вставить',
            command=self.add_entry_button_handler
        )
        self.del_entry_button = ttk.Button(
            self.edit_buttons_frame, text='Удалить',
            command=self.del_entry_button_handler
        )
        self.init_dynamic_entries()
        self.pack()

    def init_dynamic_entries(self):
        '''
        Создание динамических виджетов
        '''
        entries_dict = dict()
        for field in self.table.description:
            entries_dict[field] = dict()
            entries_dict[field]['table'] = self.table.name
            entries_dict[field]['field'] = field
            entries_dict[field]['name'] = field
            entries_dict[field]['type'] = 'list'
        self.dynamic_entries = \
            DynamicEntries(
                entries_dict,
                self.table_dynamic_entries_frame
                )

    def pack(self):
        '''
        В этот метод вынесена упаковка виджетов на родительском виджете
        '''
        self.tables_frame.pack(fill='x', padx=self.padding, pady=self.padding)
        self.tables_list.pack(fill='x', padx=self.padding, pady=self.padding)
        self.edit_frame.pack(fill='x', padx=self.padding, pady=self.padding)
        self.table_dynamic_entries_frame.pack(fill='x')
        self.edit_buttons_frame.pack()
        self.add_entry_button.pack(
            side='left',
            padx=self.padding,
            pady=self.padding
            )
        self.del_entry_button.pack(padx=self.padding, pady=self.padding)

    def tables_list_handler(self, event):
        '''
        Обёртка-обработчик быбора пресета из списка
        '''
        self.refresh_table_view()

    def refresh_table_view(self):
        '''
        Обновление таблицы, выводимой на главном текстовом поле, а так же
        обновление динамических виджетов
        '''
        self.table = Table(self.tables_list.get())
        self.update_text(self.table.make_output())
        self.dynamic_entries.purge()
        self.init_dynamic_entries()

    def handle_entry(self, method):
        '''
        Метод обращается к базе, делая запрос на удаление или добавление данных
        '''
        conditions = dict()
        entries = self.dynamic_entries.entries
        for key in entries:
            value = entries[key].get()
            if value:
                if not value.isdigit():
                    value = "'{}'".format(value)
                conditions[key] = value
        error_msg = method(conditions)
        if not error_msg:
            self.refresh_table_view()
        else:
            messagebox.showerror(*error_msg)

    def add_entry_button_handler(self):
        '''
        Обёртка-обработчик кнопки добавления
        '''
        self.handle_entry(self.table.insert)

    def del_entry_button_handler(self):
        '''
        Обёртка-обработчик кнопки удаления
        '''
        self.handle_entry(self.table.delete)

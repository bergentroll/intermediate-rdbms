# Copyright (c) 2018 Anton Karmanov
# This file is a part of undergraduate's thesis.

all: clean fill

schema: make-db.sql
	sqlite3 playlist.db < make-db.sql

fill: fill-db.sql schema
	sqlite3 playlist.db < fill-db.sql

clean:
	rm -f playlist.db
